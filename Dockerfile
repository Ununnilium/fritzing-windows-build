FROM debian:bookworm

ENV COMMIT_MXE fca881fdf19405f80252967c97590976f2c2b570
ENV COMMIT_QUAZIP 9d3aa3ee948c1cde5a9f873ecbc3bb229c1182ee
ENV COMMIT_SVGPP f460b2c7ceba92a875c0ba5c333826652863b396
ARG TARGET x86_64-w64-mingw32.static
ENV INSTALL_PREFIX /src/mxe/usr/$TARGET

RUN apt-get update && \
    export DEBIAN_FRONTEND=noninteractive && \
     apt-get install -y \
        autoconf \
        automake \
        autopoint \
        bash \
        bison \
        bzip2 \
        flex \
        g++ \
        g++-multilib \
        gettext \
        git \
        gperf \
        intltool \
        libc6-dev-i386 \
        libgdk-pixbuf2.0-dev \
        libltdl-dev \
        libgl-dev \
        libpcre3-dev \
        libssl-dev \
        libtool-bin \
        libxml-parser-perl \
        lzip \
        make \
        openssl \
        p7zip-full \
        patch \
        perl \
        python3 \
        python3-distutils \
        python3-mako \
        python3-pkg-resources \
        python-is-python3 \
        ruby \
        sed \
        unzip \
        wget \
        xz-utils && \
    mkdir -p /src && \
    cd /src && \
    git clone https://github.com/mxe/mxe.git && \
    cd mxe && \
    git checkout ${COMMIT_MXE} && \
    make MXE_TARGETS=$TARGET qt6-qtbase qt6-qtserialport qt6-qtsvg qt6-qt5compat boost libgit2 libjpeg-turbo libpng openssl zlib --jobs=2 JOBS=`nproc`



RUN export PATH=/src/mxe/usr/bin:$PATH && \
    cd /src && \
    git clone https://github.com/svgpp/svgpp && \
    cd /src/svgpp && \
    git checkout ${COMMIT_SVGPP} && \
    mv include/* $INSTALL_PREFIX/include && \
    cd /src && \
    rm -r /src/svgpp && \
    wget -O - https://sourceforge.net/projects/ngspice/files/ng-spice-rework/42/ngspice-42.tar.gz | tar xz && \
    cd /src/ngspice-42 && \
    sed -i 's/#if defined(HAVE__PROC_MEMINFO)/#if defined(HAVE__PROC_MEMINFO_INVALID)/' src/frontend/get_resident_set_size.c && \
    ./configure --host=$TARGET --enable-static --disable-shared && \
    make -j`nproc` && \
    cd /src && \
    git clone https://github.com/stachenov/quazip && \
    cd /src/quazip && \
    git checkout ${COMMIT_QUAZIP} && \
    mkdir build && \
    cd build && \
    $TARGET-cmake  -DCMAKE_INSTALL_PREFIX:PATH=$INSTALL_PREFIX -DQUAZIP_QT_MAJOR_VERSION=6 .. && \
    make -j`nproc` && \
    make install && \
    rm -r /src/quazip && \
    mkdir /src/clipper && \
    cd /src/clipper && \
    wget -O clipper.zip https://sourceforge.net/projects/polyclipping/files/clipper_ver6.4.2.zip/download && \
    unzip clipper.zip && \
    cd cpp && \
    $TARGET-cmake -DCMAKE_INSTALL_PREFIX:PATH=$INSTALL_PREFIX . && \
    make -j`nproc` && \
    make install && \
    rm -r /src/clipper && \
    mkdir /src/src

ENV PATH /src/mxe/usr/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
COPY build_fritzing.sh /usr/local/bin/build-fritzing
COPY build_fritzing.sh Dockerfile Readme.md /src/src
RUN chmod +x /usr/local/bin/build-fritzing
CMD ["/usr/local/bin/build-fritzing"]
