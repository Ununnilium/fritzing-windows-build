# Fritzing Windows Builds
Fritzing as a 32 bit and 64 bit x64 portable application (https://github.com/fritzing/fritzing-app, https://fritzing.org) cross-compiled for Windows with a Linux Docker container.

## Usage
First a Docker image must be built:
```bash
docker build --build-arg x86_64-w64-mingw32.static -t fritzing-build-win:x86_64 .
docker build --build-arg i686-w64-mingw32.static -t fritzing-build-win:x86 .
```
Then, Fritzing can be compiled. A ZIP file is created in /build, therefore /build should be a local directory mounted as volume:
```bash
docker run --rm -v build:/build fritzing-build-win:x86_64
```
The result is then in build/fritzing.zip.

To use a specific fritzing-app commit, use the enviromental variable COMMIT_FRITZING_APP:
```bash
docker run --rm -e COMMIT_FRITZING_APP=8f5f1373835050ce014299c78d91c24beea9b633 -v build:/build fritzing-build-win
```

## License
This repository uses the _ISC_ license, but Fritzing uses _GPL v3_ for the application and _Creative Commons Attribution-ShareALike 3.0 Unported_ for the components. Libraries used by Fritzing are under less strict licenses, e.g. Qt uses _LGPL v3_. As far as I understand (see here https://stackoverflow.com/a/15322678/2372674), the resulting statically linked binary can legally be distributed because all the source code to build it is available on https://gitlab.com/Ununnilium/fritzing-windows-build and https://github.com/fritzing/fritzing-app.
