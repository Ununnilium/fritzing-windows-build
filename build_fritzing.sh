#!/bin/sh
set -e

# Tested with commit dbdbe34c843677df721c7b3fc3e32c0f737e7e95: https://github.com/fritzing/fritzing-app/tree/dbdbe34c

cd /src && \
git clone https://github.com/fritzing/fritzing-app && \
cd /src/fritzing-app && \
if [ -n "$1" ]; then
  echo "Building Fritzing for commit $COMMIT_FRIZTING_APP ..." && \
  git checkout ${COMMIT_FRIZTING_APP}
else
  echo "COMMIT_FRIZTING_APP not set, building Fritzing with latest commit ..."
fi
sed -i 's/QT_MOST=.*/QT_MOST=6.999.0/' phoenix.pro && \
mkdir /src/fritzing-app/build && \
cd /src/fritzing-app/build && \
echo 'LIBS += -lgit2 -lrpcrt4 -lcrypt32 -lole32 -lssh2 -lz -lws2_32 -lbcrypt' >  ../pri/libgit2detect.pri && \
echo "INCLUDEPATH += ${INSTALL_PREFIX}/include/QuaZip-Qt6-1.4 ${INSTALL_PREFIX}/include/QuaZip-Qt6-1.4/quazip" >  ../pri/quazipdetect.pri && \
echo 'LIBS += -lquazip1-qt$$QT_MAJOR_VERSION' >> ../pri/quazipdetect.pri && \
echo "QMAKE_CXXFLAGS += -DQUAZIP_STATIC" >> ../pri/quazipdetect.pri && \
sed -i 's/win32/win32-msvc*/g' ../pri/gitversion.pri && \
echo "INCLUDEPATH += /src/ngspice-42/src/include" > ../pri/spicedetect.pri && \
echo "INCLUDEPATH += ${INSTALL_PREFIX}/include/polyclipping" >  ../pri/clipper1detect.pri && \
echo "LIBS += -lpolyclipping" >>  ../pri/clipper1detect.pri && \
sed -i 's/(long) dynamic_cast<const QGraphicsItem/(intptr_t) dynamic_cast<const QGraphicsItem/g' /src/fritzing-app/src/items/itembase.cpp && \
sed -i -e 's/arg((long) pitem/arg((intptr_t) pitem/' -e 's/arg((long) itemBase/arg((intptr_t) itemBase/' /src/fritzing-app/src/sketch/sketchwidget.cpp && \
sed -i 's/arg((long) this/arg((intptr_t) this/g' /src/fritzing-app/src/connectors/connectoritem.cpp && \
$INSTALL_PREFIX/qt6/bin/qmake ../phoenix.pro && \
make -j`nproc` && \
mkdir -p /tmp/fritzing/bin && \
mkdir -p /build && \
mv ../release32/Fritzing.exe /tmp/fritzing/bin && \
cd /tmp/fritzing && \
cp -r /src/src . && \
git clone https://github.com/fritzing/fritzing-parts && \
cd /tmp && \
zip -r /build/fritzing.zip fritzing && \
rm -r /tmp/fritzing && \
echo "Fritzing packed in /build/fritzing.zip"
